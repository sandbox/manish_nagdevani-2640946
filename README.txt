Introduction
============

Module name		: 	Taxonomy root leaf mapper
Info			:	Updates a table for selected vocabularies on
                                insert/delete/update of taxonomy terms.
				The table contains mapping of leaf_tid vs
                                root_tid.
Url to sync manually	:	[site-url]/sync/taxonomy-flat-table

FEATURES
============

Sync Manually 		: 	Sync entire taxonomy flat table.
Procedure		: 	Enter the url : 
                                [site-url]/sync/taxonomy-flat-table

Sync on term insert	:	Whenever a new term is added in any category 
                                level, mc_category_flat_table is updated 
                                automatically.
Procedure		:	Add a new taxonomy term in mc_category 
                                vocabulary.

Sync on term delete	:	Whenever a term is deleted, 
                                mc_category_flat_table is updated automatically.
Procedure		:	Add a any taxonomy term from mc_category 
                                vocabulary.

Sync on update		:	Whenever a term is updated(its parent is 
                                changed), mc_category_flat_table is updated 
                                automatically.  
Procedure		:	Update any taxonomy term from mc_category 
                                vocabulary.

INSTALLATION
============

1. Place the module in your preferred modules directory
2. Enable the module from admin/modules
3. Goto [site-url]/mapping/taxonomy-flat-table to add the vocabularies you want 
   to sync.
4. Got to [site-url]/sync/taxonomy-flat-table to manually sync.
5. Use function
   taxonomy_root_leaf_mapper_get_leaf_tids_using_root_tid($root_tid) to
   fetch leaf level tids.
6. Use function
   taxonomy_root_leaf_mapper_get_root_tid_using_leaf_tid($leaf_tid) to fetch
   root id for leaf term.
